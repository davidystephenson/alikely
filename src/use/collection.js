import {
  useCollectionData
} from 'react-firebase-hooks/firestore'

export default function useCollection (
  collection
) {
  return useCollectionData(
    collection,
    { idField: 'id' }
  )
}
