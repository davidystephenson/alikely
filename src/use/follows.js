import useFollowers from './followers'

export default function useFollows (user) {
  const [followers] = useFollowers()

  function isFollower ({ follower }) {
    return follower.id === user.uid
  }

  if (followers) {
    return followers.some(isFollower)
  }

  return false
}
