import React from 'react'

import fireContext from '../../context/fire'

import useAuth from '../auth'

const { useContext } = React

export default function useUser (
  { uid, id } = {}, ref = {}
) {
  const { store } = useContext(fireContext)

  uid = uid || id || ref.id

  ref = ref.id || store
    .collection('user')
    .doc(uid)

  const { user } = useAuth()

  const authed = user && uid === user.uid

  return {
    authed,
    id: uid,
    uid,
    ref
  }
}
