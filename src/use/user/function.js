import React from 'react'

import userContext from '../../context/user'

import useFunction from '../function'

export default function useUserFunction (
  name
) {
  const { uid } = React
    .useContext(userContext)

  const data = { user: { uid } }

  return useFunction(name, data)
}
