import useUser from './index'

export default function useUserRef (ref) {
  console.log('ref test:', ref)
  const { id } = ref
  console.log('id test:', id)

  return useUser({ uid: id }, ref)
}
