import React from 'react'

import userContext from '../../context/user'

import useWhere from '../where'

export default function useUserItems () {
  const {
    uid
  } = React.useContext(userContext)

  return useWhere(
    'item',
    'uid',
    '==',
    uid
  )
}
