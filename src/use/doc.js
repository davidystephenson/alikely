import {
  useDocumentData
} from 'react-firebase-hooks/firestore'

export default function useDoc (doc) {
  return useDocumentData(
    doc,
    { idField: 'id' }
  )
}
