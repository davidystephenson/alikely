import { useContext } from 'react'

import fireContext from '../context/fire'

import useCollection from './collection'

export default function useMatches (uid) {
  const { store } = useContext(fireContext)

  const user = store
    .collection('user')
    .doc(uid)

  const items = store
    .collection('item')
    .where('users', 'array-contains', user)
    .where('usersCount', '>', 1)

  return useCollection(items)
}
