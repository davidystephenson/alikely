import useUserFunction from './user/function'
export default function useFollow () {
  return useUserFunction('Follow')
}
