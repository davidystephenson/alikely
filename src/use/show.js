import React from 'react'

export default function useSection () {
  const [open, setOpen] = React
    .useState(false)

  function onClick () {
    setOpen(!open)
  }

  return {
    onClick,
    open,
    setOpen
  }
}
