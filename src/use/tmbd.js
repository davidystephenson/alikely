/* global fetch */

import { tmbd } from '../../keys'

export default async function useTmbd (query) {
  const url = `https://api.themoviedb.org/3/search/multi?api_key=${tmbd}&query=${query}&include_adult=true`

  const response = await fetch(url)

  console.log('response test:', response)
}
