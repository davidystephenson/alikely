import React from 'react'

import fireContext from '../context/fire'
import userContext from '../context/user'

import useCollection from './collection'

const { useContext } = React

export default function useLists () {
  const { store } = useContext(fireContext)
  const {
    uid
  } = React.useContext(userContext)

  const lists = store
    .collection('user')
    .doc(uid)
    .collection('list')

  return useCollection(lists)
}
