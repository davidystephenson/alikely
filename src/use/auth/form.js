import { useContext, useState } from 'react'

import fireContext from '../../context/fire'

import useFunction from '../function'

export default function useAuthForm () {
  const [email, setEmail] = useState('')
  const [name, setName] = useState('')
  const [
    password, setPassword
  ] = useState('')

  const { auth } = useContext(fireContext)

  const { run } = useFunction('signUp', {
    email, name, password
  })

  async function signUp () {
    return run()
  }

  async function signIn () {
    console.log('email test:', email)
    console.log('password test:', password)
    return auth.signInWithEmailAndPassword(
      email, password
    )
  }

  async function signOut () {
    console.log('signout test')
    return auth.signOut()
  }

  return {
    email,
    name,
    password,
    setEmail,
    setName,
    setPassword,
    signIn,
    signOut,
    signUp
  }
}
