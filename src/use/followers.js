import React from 'react'

import userContext from '../context/user'
import useWhere from '../use/where'

export default function useFollows () {
  const { ref } = React
    .useContext(userContext)

  return useWhere(
    'follow', 'followed', '==', ref
  )
}
