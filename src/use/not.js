import useCollection from './collection'

function merge (high, low) {
  if (high && low) {
    return [...high, ...low]
  }

  return high || low
}

export default function useNot (
  query, field, value
) {
  const greater = query
    .where(field, '>', value)
  const high = useCollection(greater)

  const less = query.where(field, '<', value)
  const low = useCollection(less)

  const docs = merge(high[0], low[0])
  const loading = high[1] && low[1]
  const error = high[1] || low[1]

  return [docs, loading, error]
}
