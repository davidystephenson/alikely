/* global fetch */

import React from 'react'

import { tmdb } from '../keys'

import listContext from '../context/list'

import Label from '../view/Label'

import useFunction from './function'

function address (value) {
  const key = `api_key=${tmdb}`
  const query = `query=${value}`
  const adult = 'include_adult=true'
  const queries = `${key}&${query}&${adult}`
  const base = `https://api.themoviedb.org`
  const search = '3/search/multi'

  return `${base}/${search}?${queries}`
}

export default function useAdd () {
  const {
    id
  } = React.useContext(listContext)

  const [data, setData] = React.useState({})
  const [
    options, setOptions
  ] = React.useState([])
  const [value, setValue] = React.useState('')

  const sourceId = `${data.media_type}_${data.id}`

  const {
    run, loading, setStatus, status
  } = useFunction('Add', {
    listId: id, sourceId, data
  })

  async function add (event) {
    event.preventDefault()

    try {
      await run()
      setData({})
      setValue('')
    } catch (error) {
      throw error
    }
  }

  function option (value) {
    const label = Label(value)

    function set () { setData(value) }

    return { label, set, value }
  }

  async function onChange (event) {
    const { value } = event.target

    setValue(value)
    setStatus('')

    if (value) {
      const url = address(value)

      try {
        const response = await fetch(url)

        if (response.ok) {
          const json = await response.json()

          const types = ['movie', 'tv']

          const media = json
            .results
            .filter(result => types
              .includes(result.media_type)
            )

          const options = media
            .map(option)

          setOptions(options)
        } else {
          setOptions([])

          console.error(response)
        }
      } catch (error) {
        setOptions([])

        throw error
      }
    }
  }

  return {
    add,
    data,
    loading,
    onChange,
    options,
    value,
    setData,
    status
  }
}
