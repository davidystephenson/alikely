/* global fetch */

import { tmdb } from '../keys'

export default function useSelect () {
  function option (result) {
    const year = result.release_date &&
      result.release_date.length >= 4
      ? result
        .release_date
        .slice(0, 4)
      : '????'

    return {
      label: `${result.title} (${year})`,
      value: result.id
    }
  }

  async function loadOptions (value) {
    if (value) {
      const key = `api_key=${tmdb}`
      const query = `query=${value}`
      const adult = 'include_adult=true'
      const queries = `${key}&${query}&${adult}`
      const base = `https://api.themoviedb.org`
      const search = '3/search/movie'
      const url = `${base}/${search}?${queries}`
      const response = await fetch(url)

      if (response.ok) {
        const json = await response.json()

        const options = json
          .results
          .map(option)

        return options
      } else {
        console.error(
          'Error in response:',
          response
        )
      }
    }

    return []
  }

  return {
    loadOptions
  }
}
