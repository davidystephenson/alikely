import React from 'react'

import fireContext from '../context/fire'

const { useContext, useState } = React

export default function useFunction (
  name, data, start, end
) {
  const {
    functions
  } = useContext(fireContext)

  const [first] = name
  const lower = name
    .replace(first, first.toLowerCase())

  start = start || `${name}ing...`
  end = end || `${name}ed!`

  const [status, setStatus] = useState('')
  const [
    loading,
    setLoading
  ] = useState(false)

  async function run () {
    setStatus(start)
    setLoading(true)

    const call = functions.httpsCallable(lower)

    try {
      const result = await call(data)

      setStatus(end)
      setLoading(false)

      return result
    } catch (error) {
      setStatus(error.message)
      setLoading(false)

      throw error
    }
  }

  return {
    [lower]: run,
    loading,
    run,
    setStatus,
    status
  }
}
