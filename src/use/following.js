import React from 'react'

import userContext from '../context/user'

import useWhere from './where'

export default function useFollowing () {
  const { ref } = React
    .useContext(userContext)

  return useWhere(
    'follow', 'follower', '==', ref
  )
}
