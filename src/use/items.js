import useWhere from './where'

export default function useItems (list) {
  return useWhere(
    'item',
    'lists',
    'array-contains',
    list
  )
}
