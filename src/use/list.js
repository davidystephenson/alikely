/* global fetch */

import React from 'react'

import { tmdb } from '../keys'

import fireContext from '../context/fire'

import Label from '../view/Label'

import useFunction from './function'
import useItems from './items'

const { useState, useContext } = React

function address (value) {
  const key = `api_key=${tmdb}`
  const query = `query=${value}`
  const adult = 'include_adult=true'
  const queries = `${key}&${query}&${adult}`
  const base = `https://api.themoviedb.org`
  const search = '3/search/multi'

  return `${base}/${search}?${queries}`
}

export default function useList (
  { uid },
  { id }
) {
  const { store } = useContext(fireContext)

  const list = store
    .collection('user')
    .doc(uid)
    .collection('list')
    .doc(id)

  const items = useItems(list)

  const [value, setValue] = useState('')

  const {
    run, loading, status
  } = useFunction('Add', {
    listId: id,
    id: value.id,
    data: value
  })

  async function add (event) {
    event.preventDefault()

    try {
      await run()
    } catch (error) {
      throw error
    }

    setValue('')
  }

  function option (result) {
    const label = Label(result)

    return { label, value: result }
  }

  async function loadOptions (value) {
    setValue(value)

    if (value) {
      const url = address(value)

      try {
        const response = await fetch(url)

        if (response.ok) {
          const json = await response.json()

          const types = ['movie', 'tv']

          const media = json
            .results
            .filter(result => types
              .includes(result.media_type)
            )

          const options = media
            .map(option)

          return options
        } else {
          console.error(response)
        }
      } catch (error) {
        throw error
      }
    }

    return []
  }

  function onChange (data) {
    const value = data.target
      ? data.target.value
      : data.value

    setValue(value)
  }

  return {
    add,
    id,
    items,
    loading,
    loadOptions,
    onChange,
    value,
    status
  }
}
