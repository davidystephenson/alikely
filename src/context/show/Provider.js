import React from 'react'

import context from './index'

import useShow from '../../use/show'

export default function ShowProvider ({
  children
}) {
  const show = useShow()

  return <context.Provider value={show}>
    {children}
  </context.Provider>
}
