import React from 'react'

import useFollowing from '../use/following'

import Followed from './Followed'
import Ticker from './Ticker'

export default function Following () {
  const stream = useFollowing()

  return <Ticker
    stream={stream}
    title='Follows'
    View={Followed}
  />
}
