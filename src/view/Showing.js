import React from 'react'

import Show from '../context/show/Provider'

import Section from './Section'
import Curtain from './Curtain'

export default function Showing ({
  children, title
}) {
  return <Show>
    <Section title={title}>
      <Curtain>
        {children}
      </Curtain>
    </Section>
  </Show>
}
