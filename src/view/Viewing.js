import React from 'react'

import Viewer from './Viewer'
import Showing from './Showing'

export default function Viewing ({
  stream, title, View
}) {
  return <Showing title={title}>
    <Viewer
      stream={stream}
      View={View}
    />
  </Showing>
}
