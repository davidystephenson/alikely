import React from 'react'

import userContext from '../context/user'

import useUser from '../use/user'

import Follow from './Follow'
import Following from './Following'
import Followers from './Followers'
import Lists from './Lists'
import Showing from './Showing'

export default function User (user) {
  const data = useUser(user)

  const title = <React.Fragment>
    {user.displayName}

    <Follow />
  </React.Fragment>

  const { Provider } = userContext

  return <Provider value={data}>
    <Showing title={title}>
      <Following />

      <Followers />

      <Lists />
    </Showing>
  </Provider>
}
