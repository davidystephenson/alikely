import React from 'react'

import showContext from '../context/show'

export default function Curtain ({
  children
}) {
  const { open } = React
    .useContext(showContext)

  return open && children
}
