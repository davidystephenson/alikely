import React from 'react'

import useDoc from '../use/doc'

import Viewer from './Viewer'
import User from './User'

export default function Follower ({
  follower
}) {
  const user = useDoc(follower)

  return <Viewer
    stream={user}
    View={User}
  />
}
