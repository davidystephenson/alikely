import React, { Fragment } from 'react'

import Matches from './Matches'
import User from './User'

export default function CurrentUser (user) {
  return <Fragment>
    <User {...user} />

    <Matches {...user} />
  </Fragment>
}
