export default function Year (result) {
  const date = result.release_date ||
    result.first_air_date

  const annual = date && date.length >= 4

  if (annual) {
    return date.slice(0, 4)
  }

  return '????'
}
