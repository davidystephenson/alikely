import React, { Fragment } from 'react'

export default function Viewer ({
  debug,
  stream,
  View
}) {
  if (debug) {
    console.log('stream test:', stream)
  }

  const [ data, firing, error ] = stream

  if (firing) {
    return `Loading...`
  }

  if (error) {
    return `Error: ${error}`
  }

  if (data) {
    if (debug) {
      console.log('data test:', data)
    }

    if (View) {
      if (Array.isArray(data)) {
        const items = data.map(datum => <View
          key={datum.id}
          {...datum}
        />)

        if (debug) {
          console.log('items test:', items)
        }

        return <Fragment>
          {items}
        </Fragment>
      }

      return <View {...data} />
    }

    console.warn(
      'No View was provided for data:',
      data
    )
  }

  return null
}
