import React from 'react'
import Label from './Label'

export default function Film (result) {
  return <p>
    <Label {...result} />
  </p>
}
