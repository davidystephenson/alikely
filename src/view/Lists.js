import React from 'react'

import useLists from '../use/lists'

import Ticker from './Ticker'
import List from './List'

export default function Lists () {
  const lists = useLists()

  return <Ticker
    stream={lists}
    title='Lists'
    View={List}
  />
}
