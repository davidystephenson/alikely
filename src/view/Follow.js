import React from 'react'

import userContext from '../context/user'
import useFollow from '../use/follow'
import useFollows from '../use/follows'
import useAuth from '../use/auth'
import useUserFunction from '../use/user/function'

export default function Follow () {
  const {
    authed
  } = React.useContext(userContext)

  const { user } = useAuth()
  const follow = useFollow()
  const unfollow = useUserFunction('Unfollow')

  const follows = useFollows(user)

  const status = follow.loading
    ? follow.status
    : unfollow.loading
      ? unfollow.status
      : null

  if (!authed) {
    if (status) {
      return status
    }

    if (follows) {
      return <button onClick={unfollow.run}>
        Unfollow
      </button>
    }

    return <button onClick={follow.run}>
      Follow
    </button>
  }

  return null
}
