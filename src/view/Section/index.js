import React from 'react'

import showContext from '../../context/show'

import './style.css'

export default function Section ({
  title,
  debug,
  children
}) {
  const { onClick } = React
    .useContext(showContext)

  if (debug) {
    console.log('title test:', title)
    console.log('children test:', children)
  }

  return <details>
    <summary onClick={onClick}>
      {title}
    </summary>

    {children}
  </details>
}
