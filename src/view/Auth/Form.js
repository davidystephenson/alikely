import React from 'react'

import fireContext from '../../context/fire'

import useAuth from '../../use/auth'
import useAuthForm from '../../use/auth/form'

import Field from '../Field'
import SignOut from '../SignOut'

export default function Auth () {
  const { auth } = React
    .useContext(fireContext)
  const { user } = useAuth(auth)

  const {
    email,
    password,
    setEmail,
    setPassword
  } = useAuthForm()

  async function signUp (event) {
    event.preventDefault()

    console.log('email test:', email)
    console.log('password test:', password)

    const response = await auth
      .createUserWithEmailAndPassword(
        email, password
      )

    console.log('response test:', response)
  }

  console.log('user test:', user)

  if (user) {
    return <SignOut />
  } else {
    return <form onSubmit={signUp}>
      <Field
        placeholder='Email'
        onChange={setEmail}
        value={email}
      />

      <Field
        placeholder='Password'
        onChange={setPassword}
        value={password}
      />

      <button>Sign Up</button>
    </form>
  }
}
