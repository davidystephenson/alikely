import React, {
  useContext, Fragment
} from 'react'

import fireContext from '../../context/fire'

import useAuth from '../../use/auth'

import SignIn from '../SignIn'
import SignOut from '../SignOut'
import SignUp from '../SignUp'

export default function Auth () {
  const { auth } = useContext(fireContext)
  const { user } = useAuth(auth)

  if (user) {
    return <SignOut />
  } else {
    return <Fragment>
      <SignUp />
      <SignIn />
    </Fragment>
  }
}
