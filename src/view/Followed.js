import React from 'react'

import useDoc from '../use/doc'

import Viewer from './Viewer'
import User from './User'

export default function Followed ({
  followed
}) {
  const user = useDoc(followed)

  return <Viewer
    stream={user}
    View={User}
  />
}
