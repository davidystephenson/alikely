function Content (result) {
  const { origin_country: origin } = result
  const country = origin && origin[0]

  const language = result.original_language

  if (country || language) {
    if (language === 'en') {
      return country
    }

    if (country && language) {
      return `${language} ${country}`
    }

    return country || language
  }
}

export default function Country (result) {
  const content = Content(result)

  if (content) {
    return `[${content}]`
  }

  return ''
}
