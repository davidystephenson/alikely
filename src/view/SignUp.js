import React from 'react'

import useAuthForm from '../use/auth/form'

import Field from './Field'

export default function SignUp () {
  const {
    email,
    name,
    password,
    setEmail,
    setName,
    setPassword,
    signUp
  } = useAuthForm()

  async function onSubmit (event) {
    event.preventDefault()

    const result = await signUp()

    console.log(
      'onSubmit result test:',
      result
    )
  }

  return <form onSubmit={onSubmit}>
    <Field
      placeholder='Email'
      onChange={setEmail}
      value={email}
    />

    <Field
      placeholder='Name'
      onChange={setName}
      value={name}
    />

    <Field
      placeholder='Password'
      onChange={setPassword}
      value={password}
    />

    <button>Sign Up</button>
  </form>
}
