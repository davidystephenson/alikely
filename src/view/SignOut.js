import React from 'react'
import useAuthForm from '../use/auth/form'

export default function SignOut () {
  const { signOut } = useAuthForm()

  return <button onClick={signOut}>
    Sign out
  </button>
}
