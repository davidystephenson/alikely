import React from 'react'

import listContext from '../context/list'
import userContext from '../context/user'

import useList from '../use/list'

import Items from './Items'
import Showing from './Showing'
import Summary from './Summary'

export default function List (list) {
  const {
    uid
  } = React.useContext(userContext)

  const data = useList({ uid }, list)

  const { id, items } = data

  const [docs] = items
  const title = <Summary
    title={id}
    docs={docs}
  />

  return <listContext.Provider value={data}>
    <Showing title={title}>
      <Items />
    </Showing>
  </listContext.Provider>
}
