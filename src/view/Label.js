import Year from './Year'
import Country from './Country'

export default function Label (result) {
  const title = result.title || result.name
  const year = Year(result)
  const country = Country(result)

  return `${title} (${year}) ${country}`
}
