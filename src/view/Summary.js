import React, { Fragment } from 'react'

export default function Summary ({
  docs, title
}) {
  if (docs) {
    return <Fragment>
      {title} ({docs.length})
    </Fragment>
  }

  return title
}
