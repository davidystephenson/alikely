import React from 'react'

import userContext from '../context/user'

import useMatches from '../use/matches'
import useUser from '../use/user'

import Match from './Match'
import Ticker from './Ticker'

export default function Matches (user) {
  const data = useUser(user)

  const matches = useMatches(user.uid)

  const { Provider } = userContext

  return <Provider value={data}>
    <Ticker
      stream={matches}
      title='Matches'
      View={Match}
    />
  </Provider>
}
