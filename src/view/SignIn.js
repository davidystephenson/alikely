import React from 'react'

import useAuthForm from '../use/auth/form'

import Field from './Field'

export default function SignUp () {
  const {
    email,
    password,
    setEmail,
    setPassword,
    signIn
  } = useAuthForm()

  async function onSubmit (event) {
    event.preventDefault()

    try {
      const result = await signIn()

      console.log(
        'onSubmit result test:',
        result
      )
    } catch (error) {
      throw error
    }
  }

  return <form onSubmit={onSubmit}>
    <Field
      placeholder='Email'
      onChange={setEmail}
      value={email}
    />

    <Field
      placeholder='Password'
      onChange={setPassword}
      value={password}
    />

    <button>Sign In</button>
  </form>
}
