import React from 'react'

import useFollowers from '../use/followers'

import Follower from './Follower'
import Ticker from './Ticker'

export default function Followers () {
  const followers = useFollowers()

  return <Ticker
    stream={followers}
    title='Followers'
    View={Follower}
  />
}
