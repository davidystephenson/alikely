import React, { Fragment } from 'react'

import CurrentUser from './CurrentUser'
import Users from './Users'

export default function Main (user) {
  return <Fragment>
    <CurrentUser {...user} />

    <Users />
  </Fragment>
}
