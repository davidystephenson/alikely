import React from 'react'

import useAuth from '../use/auth'
import Viewer from './Viewer'

import Auth from './Auth'
import Main from './Main'

function App () {
  const { state } = useAuth()

  return <main>
    <Auth />

    <Viewer
      stream={state}
      View={Main}
    />
  </main>
}

export default App
