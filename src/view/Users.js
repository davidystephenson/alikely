import React from 'react'

import fireContext from '../context/fire'

import useCollection from '../use/collection'
import Ticker from './Ticker'
import User from './User'

const { useContext } = React

export default function Users () {
  const { store } = useContext(fireContext)

  const collection = store
    .collection('user')
  const users = useCollection(collection)

  return <Ticker
    stream={users}
    View={User}
    title='Users'
  />
}
