import React from 'react'

import listContext from '../context/list'

import useAuth from '../use/auth'

import Add from './Add'
import Film from './Film'
import Viewer from './Viewer'

const { useContext, Fragment } = React

export default function Items () {
  const { items } = useContext(listContext)

  const authState = useAuth()
  const { authed } = authState
  const add = authed && <Add />

  return <Fragment>
    {add}

    <Viewer
      stream={items}
      View={Film}
    />
  </Fragment>
}
