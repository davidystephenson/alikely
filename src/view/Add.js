import React from 'react'

import useAdd from '../use/add'

export default function Add () {
  const {
    add,
    data,
    loading,
    onChange,
    options,
    status,
    value
  } = useAdd()

  function Option ({
    label, set, value: { id }
  }) {
    return <div key={id}>
      <label>
        <input
          onChange={set}
          checked={data.id === id}
          type='radio'
        />
        {label}
      </label>
    </div>
  }

  const radios = options.map(Option)

  const button = !loading && data.id
    ? <button>Add</button>
    : status

  return <form onSubmit={add}>
    <input
      type='text'
      value={value}
      onChange={onChange}
    />

    {radios}

    {button}
  </form>
}
