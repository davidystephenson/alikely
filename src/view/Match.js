import React, { useContext } from 'react'

import userContext from '../context/user'

import Label from './Label'
import Showing from './Showing'
import Summary from './Summary'

export default function Match (item) {
  const current = useContext(userContext)
  const { users } = item

  const names = users
    .filter(user => user.id !== current.id)
    .map(user => user.id)

  const label = <Label {...item} />

  const summary = <Summary
    docs={names}
    title={label}
  />

  return <Showing title={summary}>
    {names}
  </Showing>
}
