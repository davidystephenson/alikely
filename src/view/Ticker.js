import React from 'react'

import Viewing from './Viewing'
import Summary from './Summary'

export default function Ticker ({
  title, stream, View
}) {
  const [docs] = stream

  const summary = <Summary
    docs={docs}
    title={title}
  />

  return <Viewing
    stream={stream}
    title={summary}
    View={View}
  />
}
