const functions = require('firebase-functions')
const add = require('./add')
const createUser = require('./createUser')
const follow = require('./follow')
const signUp = require('./signUp')
const unfollow = require('./unfollow')

const { auth, https: { onCall } } = functions
const user = auth.user()

exports.createUser = user.onCreate(createUser)
exports.add = onCall(add)
exports.follow = onCall(follow)
exports.signUp = onCall(signUp)
exports.unfollow = onCall(unfollow)
