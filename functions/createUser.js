const admin = require('firebase-admin')
const store = require('./store')
const upset = require('./upset')
const biographize = require('./biographize')

module.exports = async function createUser (
  { uid }
) {
  try {
    const record = await admin
      .auth()
      .getUser(uid)
    const biography = biographize(record)

    const user = store
      .collection('user')
      .doc(uid)

    await upset(user, biography)

    const film = user
      .collection('list')
      .doc('Film')

    return upset(film, { source: 'tmbd' })
  } catch (error) {
    throw error
  }
}
