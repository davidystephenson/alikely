const {
  https: { HttpsError }
} = require('firebase-functions')

const upset = require('./upset')
const checkFollow = require('./check/follow')

module.exports = async function follow (
  { user: { uid: followedId } },
  { auth: { uid: followerId } }
) {
  try {
    const {
      doc,
      followed,
      follower,
      follows
    } = await checkFollow(
      followerId,
      followedId,
      'follower'
    )

    if (follows) {
      throw new HttpsError(
        'failed-precondition',
        'You already follow that user.'
      )
    }

    return upset(doc, {
      followed: followed.ref,
      follower: follower.ref
    })
  } catch (error) {
    throw error
  }
}
