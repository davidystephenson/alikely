const {
  https: { HttpsError }
} = require('firebase-functions')

const checkFollow = require('./check/follow')

module.exports = async function follow (
  { user: { uid: followedId } },
  { auth: { uid: followerId } }
) {
  try {
    const {
      doc, follows
    } = await checkFollow(
      followerId,
      followedId,
      'followed'
    )

    if (follows) return doc.delete()

    throw new HttpsError(
      'not-found',
      'You do not follow that user.'
    )
  } catch (error) {
    throw error
  }
}
