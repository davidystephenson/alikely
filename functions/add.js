const {
  https: { HttpsError }
} = require('firebase-functions')
const {
  firestore: { FieldValue }
} = require('firebase-admin')

const store = require('./store')
const upset = require('./upset')

const checkId = require('./check/id')
const checkDoc = require('./check/doc')
const checkCurrent = require(
  './check/current'
)

module.exports = async function add (
  { listId, sourceId, data = {} },
  { auth: { uid } }
) {
  sourceId = checkId(sourceId, 'sourceId')

  try {
    const {
      ref: user
    } = await checkCurrent(uid)

    const {
      doc: listDoc,
      ref: list
    } = await checkDoc(
      listId,
      user.collection('list'),
      'list',
      'Does it belong to you?'
    )

    const { source } = listDoc.data()

    const itemId = `${source}_${sourceId}`
    const item = store
      .collection('item')
      .doc(itemId)

    const itemDoc = await item.get()

    if (itemDoc.exists) {
      const {
        lists, users, usersCount
      } = itemDoc.data()

      console.log('lists test:', typeof lists)
      console.log('users test:', users)

      if (lists.includes(list)) {
        throw new HttpsError(
          'already-exists',
          'That item is already in your list.'
        )
      }

      const count = users.includes(user)
        ? usersCount
        : usersCount + 1

      return upset(item, {
        lists: FieldValue.arrayUnion(list),
        users: FieldValue.arrayUnion(user),
        usersCount: count
      })
    }

    return upset(item, {
      ...data,
      lists: [list],
      source,
      sourceId,
      users: [user],
      usersCount: 1
    })
  } catch (error) {
    throw error
  }
}
