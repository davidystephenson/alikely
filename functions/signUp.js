const {
  https: { HttpsError }
} = require('firebase-functions')
const admin = require('firebase-admin')

const biographize = require('./biographize')
const store = require('./store')

module.exports = async function signUp (
  { email, name, password }, auth
) {
  if (auth.uid) {
    throw new HttpsError(
      'failed-precondition',
      'You are already a user.'
    )
  }

  try {
    const user = store
      .collection('user')
      .doc(name)
    const existing = await user.get()

    if (existing.exists) {
      throw new HttpsError(
        'already-exists',
        'That name is taken.'
      )
    }

    const data = {
      email,
      displayName: name,
      uid: name,
      password
    }
    const record = await admin
      .auth()
      .createUser(data)
    const biography = biographize(record)

    return biography
  } catch (error) {
    const emailExists = error.errorInfo &&
      error.errorInfo.code &&
      error
        .errorInfo
        .code === 'auth/email-already-exists'

    if (emailExists) {
      throw new HttpsError(
        'already-exists',
        'That email is taken.'
      )
    }

    throw error
  }
}
