const {
  firestore: { FieldValue }
} = require('firebase-admin')

module.exports = async function upset (
  doc, data = {}, debug
) {
  if (debug) {
    console.log('doc test:', doc)
    console.log('data test:', data)
  }
  const time = FieldValue.serverTimestamp()

  const update = { updatedAt: time }
  const updated = { ...data, ...update }

  try {
    const snapshot = await doc.get()

    if (snapshot.exists) {
      return doc.update(updated)
    } else {
      const create = { createdAt: time }
      const created = {
        ...updated,
        ...create
      }

      console.log('created test:', created)

      return doc.set(created)
    }
  } catch (error) {
    throw error
  }
}
