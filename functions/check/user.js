const store = require('../store')
const checkId = require('./id')
const checkDoc = require('./doc')

module.exports = async function checkUser (
  uid, name = 'user', message
) {
  uid = checkId(uid, 'uid')

  const users = store.collection('user')

  try {
    return checkDoc(
      uid,
      users,
      name,
      message
    )
  } catch (error) {
    throw error
  }
}
