const checkUser = require('./user')

module.exports = async function checkCurrent (
  uid
) {
  try {
    return await checkUser(
      uid,
      'current user',
      'Are you logged in?'
    )
  } catch (error) {
    throw error
  }
}
