const {
  https: { HttpsError }
} = require('firebase-functions')
const checkId = require('./id')

module.exports = async function checkDoc (
  id, collection, name, explanation
) {
  id = checkId(id, name)

  const ref = collection.doc(id)

  try {
    const doc = await ref.get()

    if (!doc.exists) {
      const statement = `${name} not found.`
      const message = explanation
        ? `${statement} ${explanation}`
        : statement

      throw new HttpsError(
        'not-found',
        message
      )
    }

    return { ref, doc, id }
  } catch (error) {
    throw error
  }
}
